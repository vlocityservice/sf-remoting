declare module 'sf-remoting' {

    export var sfRemoting:ng.IModule;
    export class SfRemotingUtils {
        constructor( $q: ng.IQService, $log: ng.ILogService);
        /**
         * This is our exposed entry point to call a salesforce @RemoteAction 
         * annotated method and exchange it for a promise.
         * 
         * @param {() => void} fn - The JavaScript reference to the remote action, the apex controller must be included on the page.
         * @param {any[]} [args=[]] - Represent the parameters that will be passed to the remote method.
         * @returns {ng.IPromise<any>}
         * @memberof SfRemotingUtils
         */
        callRemoteFn():ng.IPromise<any>
    }
}



