import {sfRemoting} from './sf-remoting.module';

export class SfRemotingUtils {
	static $inject = ['$q','$log'];
	constructor(private $q: ng.IQService, private $log: ng.ILogService) {}

	/**
	 * This is our exposed entry point to call a salesforce @RemoteAction 
	 * annotated method and exchange it for a promise.
	 * 
	 * @param {() => void} fn - The JavaScript reference to the remote action, the apex controller must be included on the page.
	 * @param {any[]} [args=[]] - Represent the parameters that will be passed to the remote method.
	 * @returns {ng.IPromise<any>}
	 * @memberof SfRemotingUtils
	 */
	public callRemoteFn(fn: () => void, args: any[] = []):ng.IPromise<any> {
		if (typeof fn !== 'function') {
			return this.$q.reject(this.handleError({message: 'RemoteAtion not defined'}))
		} else {
			args.push(this.handleResult);
			fn.apply(this, args);
		}
	}

	private handleResult(result, evt) {
		if (evt.status) {
			return this.$q.resolve(this.handleSuccess(result, evt));
		} else {
			return this.$q.reject(this.handleError(evt, evt));
		}
	}

	private handleSuccess(result, evt) {
		this.$log.debug( evt.method + ' Success: ', result, evt);
		return this.$q.resolve(result);
	}

	private handleError(reason: any, evt?: any):ng.IPromise<any> {
		this.$log.error(reason.method + ' Errror: '+  reason.message, reason, evt);
		return this.$q.reject(reason);
	}
}

sfRemoting.service('sfRemotingUtils', SfRemotingUtils);