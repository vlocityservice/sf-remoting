# SF Remoting Utils

SF Remoting utils is an angular service wich takes a call to an Apex remote method, and returns a promise

## Usage:
```
// include the dependancy
angular
	.module('foo', ['sf.remoting'])
```

```
//inject the service
angular
	.module('foo')
	.controller('fooController', fooController);
	// and make a call to a remote method!
	fooController.$inject = ['sfRemotingUtils'];
	function fooController(sfRemotingUtils) {
		sfRemotingUtils.callRemoteFn(ApexController.SomeMethod, [arg1, arg2])
			.then(function(results) {
				results will be what ever is returned from your @RemoteAction
			});
	}
```

Now to return multiple promises into one callback.

```
// you must inject angular's $q service
fooController.$inject = ['$q', 'sfRemotingUtils'];
function fooController($q, sfRemotingUtils) {
...
	// this is going to be an array of all promises to be executed, and then returned.
	var promises = [];
	angular.forEach(someCollection, funciton(value, key) {
		promises.push(sfRemotingUtils.callRemoteFn(ApexController.SomeMethod), [value]);
	});

	$q.all(promises)
		.then(function(result) {
			// result will contain containing all of our resolved promises.
		})
		.catch(function(err) {
			// do something with an error
		})
...
```

## Build

Esnure you have typescript installed.

```
npm i -g typescrpt
```

Build typescript
```
npm run build
# tsc -p tsconfig.json

#or
npm run develop
#tsc -p tsconfig.json --watch
```
