"use strict";
exports.__esModule = true;
var sf_remoting_module_1 = require("./sf-remoting.module");
var SfRemotingUtils = (function () {
    function SfRemotingUtils($q, $log) {
        this.$q = $q;
        this.$log = $log;
    }
    /**
     * This is our exposed entry point to call a salesforce @RemoteAction
     * annotated method and exchange it for a promise.
     *
     * @param {() => void} fn - The JavaScript reference to the remote action, the apex controller must be included on the page.
     * @param {any[]} [args=[]] - Represent the parameters that will be passed to the remote method.
     * @returns {ng.IPromise<any>}
     * @memberof SfRemotingUtils
     */
    SfRemotingUtils.prototype.callRemoteFn = function (fn, args) {
        if (args === void 0) { args = []; }
        if (typeof fn !== 'function') {
            return this.$q.reject(this.handleError({ message: 'RemoteAtion not defined' }));
        }
        else {
            args.push(this.handleResult);
            fn.apply(this, args);
        }
    };
    SfRemotingUtils.prototype.handleResult = function (result, evt) {
        if (evt.status) {
            return this.$q.resolve(this.handleSuccess(result, evt));
        }
        else {
            return this.$q.reject(this.handleError(evt, evt));
        }
    };
    SfRemotingUtils.prototype.handleSuccess = function (result, evt) {
        this.$log.debug(evt.method + ' Success: ', result, evt);
        return this.$q.resolve(result);
    };
    SfRemotingUtils.prototype.handleError = function (reason, evt) {
        this.$log.error(reason.method + ' Errror: ' + reason.message, reason, evt);
        return this.$q.reject(reason);
    };
    SfRemotingUtils.$inject = ['$q', '$log'];
    return SfRemotingUtils;
}());
exports.SfRemotingUtils = SfRemotingUtils;
sf_remoting_module_1.sfRemoting.service('sfRemotingUtils', SfRemotingUtils);
